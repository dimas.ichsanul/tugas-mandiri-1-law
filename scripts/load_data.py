from main.models import Anime, Manga
import csv


def run():
    with open('static/data/anime.csv') as file:
        reader = csv.reader(file)
        next(reader)  # Advance past the header

        Anime.objects.all().delete()

        for row in reader:
            if row[33] == '':
                continue

            if row[3] == '':
                row[3] = None

            if row[6] == '':
                row[6] = None

            if row[7] == '':
                row[7] = None

            if row[8] == '':
                row[8] = None

            anime = Anime(my_anime_list_id=row[0],
                          title=row[1],
                          type=row[2],
                          score=row[3],
                          scored_by=row[4],
                          status=row[5],
                          episodes=row[6],
                          start_date=row[7],
                          end_date=row[8],
                          source=row[9],
                          image_url=row[33])
            anime.save()

        with open('static/data/manga.csv') as file:
            reader = csv.reader(file)
            next(reader)  # Advance past the header

            Manga.objects.all().delete()

            for row in reader:
                if row[25] == '':
                    continue

                if row[3] == '':
                    row[3] = None

                if row[6] == '':
                    row[6] = None

                if row[7] == '':
                    row[7] = None

                if row[8] == '':
                    row[8] = None

                if row[9] == '':
                    row[9] = None

                manga = Manga(my_anime_list_id=row[0],
                              title=row[1],
                              type=row[2],
                              score=row[3],
                              scored_by=row[4],
                              status=row[5],
                              volumes=row[6],
                              chapters=row[7],
                              start_date=row[8],
                              end_date=row[9],
                              image_url=row[25])
                manga.save()
