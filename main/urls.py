from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('anime-list/<int:page>', views.anime_list_page, name='anime-list'),
    path('manga-list/<int:page>', views.manga_list_page, name='manga-list'),
    path('favourite-anime-list/', views.favourite_anime_list_page, name='favourite-anime-list'),
    path("register/", views.register_request, name="register"),
    path("login/", views.login_request, name="login"),
    path("logout/", views.logout_request, name="logout"),
    path("chat/<int:anime_id>/", views.chat_room, name="chat"),
]
