from django.db import models
from django.contrib.auth.models import User


class Anime(models.Model):
    id = models.AutoField(primary_key=True)
    my_anime_list_id = models.IntegerField(primary_key=False)
    title = models.CharField(max_length=300)
    type = models.CharField(max_length=200)
    score = models.FloatField(null=True)
    scored_by = models.IntegerField()
    status = models.CharField(max_length=200)
    episodes = models.IntegerField(null=True)
    start_date = models.DateField(null=True)
    end_date = models.DateField(null=True)
    source = models.CharField(max_length=200)
    image_url = models.CharField(max_length=300)


class Manga(models.Model):
    id = models.AutoField(primary_key=True)
    my_anime_list_id = models.IntegerField(primary_key=False)
    title = models.CharField(max_length=300)
    type = models.CharField(max_length=200)
    score = models.FloatField(null=True)
    scored_by = models.IntegerField()
    status = models.CharField(max_length=200)
    volumes = models.IntegerField(null=True)
    chapters = models.IntegerField(null=True)
    start_date = models.DateField(null=True)
    end_date = models.DateField(null=True)
    image_url = models.CharField(max_length=300)


class FavouriteAnime(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    anime = models.ForeignKey(Anime, on_delete=models.CASCADE)


class FavouriteManga(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    manga = models.ForeignKey(Manga, on_delete=models.CASCADE)
