from django.contrib import admin
from .models import Anime, Manga, FavouriteAnime, FavouriteManga


admin.site.register(Anime)
admin.site.register(Manga)
admin.site.register(FavouriteAnime)
admin.site.register(FavouriteManga)