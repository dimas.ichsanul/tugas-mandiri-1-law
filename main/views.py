from django.contrib.auth import login, authenticate, logout
from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.forms import AuthenticationForm
from .forms import NewUserForm
from .models import Anime, Manga, FavouriteAnime, FavouriteManga


def home(request):
    title = 'Main Page'
    anime_list = Anime.objects.all()[:20]
    context = {'title': title,
               'anime_list': anime_list}
    return render(request, 'main/home.html', context)


def anime_list_page(request, page):
    title = 'Anime List'
    anime_list = Anime.objects.all()[(page - 1) * 21: page * 21]
    context = {'title': title,
               'anime_list': anime_list}
    return render(request, 'main/anime_list.html', context)


def favourite_anime_list_page(request):
    title = 'Favourite Anime List'
    user = request.user
    favourite_anime_list = FavouriteAnime.objects.filter(user=user)
    anime_list = []

    for element in favourite_anime_list:
        anime_list.append(element.anime)
    context = {'title': title,
               'anime_list': anime_list}
    return render(request, 'main/favourite_anime_list.html', context)


def manga_list_page(request, page):
    title = 'Manga List'
    manga_list = Manga.objects.all()[(page - 1) * 21: page * 21]
    context = {'title': title,
               'manga_list': manga_list}
    return render(request, 'main/manga_list.html', context)


def favourite_manga_list_page(request):
    title = 'Favourite Manga List'
    user = request.user
    favourite_manga_list = FavouriteManga.objects.filter(user=user)
    manga_list = []

    for element in favourite_manga_list:
        favourite_manga_list.append(element.manga)
    context = {'title': title,
               'manga_list': manga_list}
    return render(request, 'main/favourite_manga_list.html', context)


def register_request(request):
    if request.method == "POST":
        form = NewUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            messages.success(request, "Registration successful.")
            return redirect("main:home")
        messages.error(request, "Unsuccessful registration. Invalid information.")
    form = NewUserForm()
    return render(request=request, template_name="main/register.html", context={"register_form": form})


def login_request(request):
    if request.method == "POST":
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                messages.info(request, f"You are now logged in as {username}.")
                return redirect("main:home")
            else:
                messages.error(request, "Invalid username or password.")
        else:
            messages.error(request, "Invalid username or password.")
    form = AuthenticationForm()
    return render(request=request, template_name="main/login.html", context={"login_form": form})


def logout_request(request):
    logout(request)
    messages.info(request, "You have successfully logged out.")
    return redirect("main:home")


def chat_room(request, anime_id):
    user = request.user

    if user.is_authenticated:
        anime_title = Anime.objects.get(id=anime_id).title
        title = anime_title + ' Chat Room'
        context = {'title': title, "anime_id": anime_id}
        return render(request, "main/chat_room.html", context)

    messages.info(request, "You have to login.")
    return redirect("main:login")
