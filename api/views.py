from main.models import Anime, Manga, FavouriteAnime, FavouriteManga
from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from rest_framework import permissions
from .serializers import UserSerializer, GroupSerializer, AnimeSerializer, MangaSerializer, FavouriteAnimeSerializer, \
    FavouriteMangaSerializer
from rest_framework.response import Response
from .rabbitmq import publish


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]


class GroupViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [permissions.IsAuthenticated]


class AnimeViewSet(viewsets.ModelViewSet):
    queryset = Anime.objects.all().order_by('id')
    serializer_class = AnimeSerializer

    def create(self, request, *args, **kwargs):
        anime_data = request.data

        new_anime = Anime.objects.create(my_anime_list_id=anime_data['my_anime_list_id'], title=anime_data['title'],
                                         type=anime_data['type'], score=anime_data['score'],
                                         scored_by=anime_data['scored_by'], status=anime_data['scored_by'],
                                         episodes=anime_data['episodes'], start_date=anime_data['start_date'],
                                         end_date=anime_data['end_date'], source=anime_data['source'],
                                         image_url=anime_data['image_url'])
        new_anime.save()

        severity = "info-new"
        message = "New anime: " + new_anime.title
        publish(severity=severity, message=message)

        serializer_context = {
            'request': request,
        }
        serializer = AnimeSerializer(new_anime, context=serializer_context)
        return Response(serializer.data)


class MangaViewSet(viewsets.ModelViewSet):
    queryset = Manga.objects.all().order_by('id')
    serializer_class = MangaSerializer

    def create(self, request, *args, **kwargs):
        manga_data = request.data

        new_manga = Manga.objects.create(my_anime_list_id=manga_data['my_anime_list_id'], title=manga_data['title'],
                                         type=manga_data['type'], score=manga_data['score'],
                                         scored_by=manga_data['scored_by'], status=manga_data['scored_by'],
                                         volumes=manga_data['volumes'], chapters=manga_data['chapters'],
                                         start_date=manga_data['start_date'],end_date=manga_data['end_date'],
                                         image_url=manga_data['image_url'])
        new_manga.save()

        severity = "info-new"
        message = "New Manga: " + new_manga.title
        publish(severity=severity, message=message)

        serializer_context = {
            'request': request,
        }
        serializer = MangaSerializer(new_manga, context=serializer_context)
        return Response(serializer.data)


class FavouriteAnimeViewSet(viewsets.ModelViewSet):
    queryset = FavouriteAnime.objects.all().order_by('id')
    serializer_class = FavouriteAnimeSerializer


class FavouriteMangaViewSet(viewsets.ModelViewSet):
    queryset = FavouriteManga.objects.all().order_by('id')
    serializer_class = FavouriteMangaSerializer
