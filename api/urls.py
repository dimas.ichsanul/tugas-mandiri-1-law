from django.urls import path, include
from . import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)
router.register(r'animes', views.AnimeViewSet)
router.register(r'mangas', views.MangaViewSet)
router.register(r'favourite-animes', views.FavouriteAnimeViewSet)
router.register(r'favourite-mangas', views.FavouriteMangaViewSet)

app_name = 'api'
urlpatterns = [
    path('', include(router.urls))
]
