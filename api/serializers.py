from django.contrib.auth.models import User, Group
from main.models import Anime, Manga, FavouriteAnime, FavouriteManga
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="api:user-detail")

    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="api:group-detail")

    class Meta:
        model = Group
        fields = ['url', 'name']


class AnimeSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="api:anime-detail")

    class Meta:
        model = Anime
        fields = '__all__'


class MangaSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="api:manga-detail")

    class Meta:
        model = Manga
        fields = '__all__'


class FavouriteAnimeSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="api:favouriteanime-detail")
    user = serializers.HyperlinkedIdentityField(view_name="api:user-detail")
    anime = serializers.HyperlinkedIdentityField(view_name="api:anime-detail")

    class Meta:
        model = FavouriteAnime
        fields = '__all__'


class FavouriteMangaSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="api:favouritemanga-detail")
    user = serializers.HyperlinkedIdentityField(view_name="api:user-detail")
    manga = serializers.HyperlinkedIdentityField(view_name="api:manga-detail")

    class Meta:
        model = FavouriteManga
        fields = '__all__'
